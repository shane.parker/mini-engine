const fieldEditor = () => (field, parent) => {
  const value = (field.value ? true : false)

  const elem = document.createElement('input')
  elem.type = 'checkbox'
  elem.checked = value
  elem.addEventListener('change', e => {
    field.edit(elem.checked)
  })

  parent.appendChild(elem)
}

export default fieldEditor