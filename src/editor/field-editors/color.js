import nullish from '@util/nullish'

const valueToString = (val = '#00000000') => (typeof val !== 'string') ? 
    '#' + val.toString(16).padEnd(6,'0').substr(0,7) : val

const fieldEditor = () => (field, parent) => {
  const value = nullish(field.value, undefined)

  const bg = document.createElement('div')
  bg.className = 'colorSelectBg'

  const wrapper = document.createElement('div')
  wrapper.style.backgroundColor = valueToString(value)
  wrapper.className = 'colorWrapper'

  const elem = document.createElement('input')
  elem.type = 'color'
  elem.value = valueToString(value)
  elem.addEventListener('input', e => {
    wrapper.style.backgroundColor = elem.value
    field.edit(elem.value)
  })

  wrapper.appendChild(elem)

  parent.appendChild(bg)
  parent.appendChild(wrapper)
}

export default fieldEditor
