import nullish from '@util/nullish'

const fieldEditor = () => (field, parent, row) => {
  let selectedFiles = null
  let eventCounter = 0

  const nameInput = document.createElement('input')
  nameInput.multiple = (field.multiple === true)
  nameInput.className = 'filenameInput'
  nameInput.placeholder = 'http://domain.com/file.ext'
  nameInput.value = nullish(field.value, '')
  parent.appendChild(nameInput)

  nameInput.addEventListener('focus', () => {
    if ( selectedFiles ) {
      nameInput.value = ''
    }
  })

  nameInput.addEventListener('blur', () => {
    if ( selectedFiles ) {
      nameInput.value = selectedFiles.map(f => f.name).join(', ')
    }
  })

  nameInput.addEventListener('input', () => {
    selectedFiles = null;
    field.edit(nameInput.value)
  })

  const acceptedTypes = nullish(field.mimeTypes, [])

  const acceptedFiles = files => {
    if ( acceptedTypes.length === 0 ) {
      return files
    }

    const filtered = files.filter( f => acceptedTypes.find( t =>
        f.type.startsWith(t.replace('*', ''))) !== undefined )

    if ( (field.multiple !== true) && (filtered.length > 0) ) {
      return [filtered[0]]
    }

    return filtered
  }

  row.addEventListener('dragenter', event => {
    event.stopPropagation()
    event.preventDefault()

    eventCounter++
    row.classList.add('dragDrop')
  })

  row.addEventListener('dragover', event => {
    
    event.stopPropagation()
    event.preventDefault()
  })

  row.addEventListener('dragleave', event => {
    event.stopPropagation()
    event.preventDefault()

    eventCounter--;
    if ( eventCounter == 0 ) {
      row.classList.remove('invalidDragDrop')
      row.classList.remove('dragDrop')
    }
  })

  row.addEventListener('drop', event => {
    event.stopPropagation()
    event.preventDefault()

    eventCounter--;
    if ( eventCounter == 0 ) {
      row.classList.remove('dragDrop')

      const dt = event.dataTransfer;
      const files = Array.from(dt.files)

      selectedFiles = acceptedFiles(files)
      if ( selectedFiles.length > 0 ) {
        nameInput.value = selectedFiles.map(f => f.name).join(', ')
        field.edit(selectedFiles)
      }
    }
  })

  const fileElement = document.createElement('input')
  fileElement.type = 'file'
  fileElement.accept = acceptedTypes.join('|')
  fileElement.addEventListener('change', event => {
    selectedFiles = acceptedFiles(Array.from(fileElement.files))
    nameInput.value = selectedFiles.map(f => f.name).join(', ')
    field.edit(selectedFiles)  
  })

  const selectButton = document.createElement('button')
  selectButton.className = 'fileSelectButton button'
  selectButton.addEventListener('click', event => {
    event.preventDefault()
    fileElement.click()
  })
  parent.appendChild(selectButton)
}

export default fieldEditor
