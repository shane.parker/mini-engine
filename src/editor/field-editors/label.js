const fieldEditor = () => (field, parent) => {
  const comp = document.createElement('span')
  const txt = field.value || field.name

  comp.innerText = (typeof txt === 'string') ? txt : 'Unknown'
  parent.appendChild(comp)
}

export default fieldEditor
