import nullish from '@util/nullish'
import { safeParseFloat } from '../util'

const fieldEditor = () => (field, parent) => {
  const elem = document.createElement('input')
  elem.type = 'number'
  elem.step = nullish(field.step, 1)
  elem.value = nullish(field.value, 0)
  elem.addEventListener('input', e => {
    field.edit(safeParseFloat(elem.value, elem.value))
  })

  parent.appendChild(elem)
}

export default fieldEditor

