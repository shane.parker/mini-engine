import nullish from '@util/nullish'

const fieldEditor = () => (field, parent) => {
  let open = false

  const comp = document.createElement('div')
  comp.className = 'editorSelect'

  const optionsComp = document.createElement('ul')
  optionsComp.className = 'editorSelectDropdown'
  optionsComp.tabIndex = -1
  optionsComp.addEventListener('blur', event => {
    optionsComp.classList.remove('open')
    open = false
  })
  
  const select = label => {
    optionsComp.classList.remove('open')
    comp.innerText = label
    open = false
  }

  comp.addEventListener('click', event => {
    event.preventDefault()

    open = !open
    if ( open ) {
      optionsComp.classList.add('open')
      optionsComp.focus()
    } else {
      optionsComp.classList.remove('open')
    }
  })


  const options = Array.isArray(field.options) ? field.options : []
  optionsComp.append(...options.map( o => {
    const item = document.createElement('li')

    const val = (typeof o === 'string') ? o : o.value
    const label = nullish(o.label, val)

    if ( val === field.value ) {
      comp.innerText = label
    }

    item.innerText = label
    item.value = val
    item.addEventListener('click', e => {
      e.preventDefault()
      select(label)
      field.edit(val)
    })

    return item
  }))

  parent.appendChild(comp)
  parent.appendChild(optionsComp)
}

export default fieldEditor