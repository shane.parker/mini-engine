import nullish from '@util/nullish'

const fieldEditor = () => (field, parent) => {
  const elem = document.createElement('input')
  elem.value = nullish(field.value, '')
  elem.placeholder = nullish(field.placeholder, '...')
  elem.addEventListener('input', e => {
    field.edit(elem.value)
  })

  parent.appendChild(elem)
}

export default fieldEditor
