import nullish from '@util/nullish'

const fieldEditor = () => (field, parent) => {
  const elem = document.createElement('textarea')
  elem.placeholder = nullish(field.placeholder, "...")
  elem.rows = nullish(field.rows, 5)
  elem.innerText = field.value
  elem.addEventListener('input', e => {
    field.edit(elem.value)
  })

  parent.appendChild(elem)
}

export default fieldEditor
