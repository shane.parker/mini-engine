import { safeParseFloat } from '../util'

const fieldEditor = () => (field, parent) => {
  const value = field.value || {x: 0.0, y: 0.0, z: 0.0}
  const comps = ['x', 'y', 'z']

  comps.map( v => {
    const elem = document.createElement('input')
    elem.value = value[v] || 0.0
    elem.placeholder = v
    elem.type = 'number'
    elem.step = 0.25
    elem.addEventListener('input', e => {
      value[v] = safeParseFloat(elem.value, value[v])
      field.edit(value)
    })

    return elem
  }).forEach( e => parent.appendChild(e) )
}

export default fieldEditor
