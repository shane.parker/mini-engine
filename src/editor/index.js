import booleanEditor from './field-editors/boolean'
import labelEditor from './field-editors/label'
import stringEditor from './field-editors/string'
import vector3Editor from './field-editors/vector3'
import numberEditor from './field-editors/number'
import selectEditor from './field-editors/select'
import textEditor from './field-editors/text'
import colorEditor from './field-editors/color'
import fileEditor from './field-editors/file'

import eventListenerSet from '@util/event-listener-set'

const fieldEditorMap = {
  label: labelEditor(),
  vector3: vector3Editor(),
  boolean: booleanEditor(),
  string: stringEditor(),
  number: numberEditor(),
  select: selectEditor(),
  color: colorEditor(),
  text: textEditor(),
  file: fileEditor()
}

const editor = ({
  engine,
  visualizers,
  world,
  scheduler,
}, opts = {}) => {
  const {
    mainContentSelector = '#content',
    sectionsSelector = '#editorSections',
    fieldEditors: customEditors = {},
    toggleKey = 'Backquote'
  } = opts
  
  let {
    activate = false,
    open = false
  } = opts

  let worldSnaphot = {}
  let active = null

  const mainContent = document.querySelector(mainContentSelector)
  if ( !mainContent ) {
    throw Error(`Editor cannot find main content element: ${mainContentId}`)
  }

  const sectionsContainer = document.querySelector(sectionsSelector)
  if ( !sectionsContainer ) {
    throw Error(`Editor cannot find sections container div: ${sectionsId}`)
  }

  let editing = null
  Object.assign(fieldEditorMap, customEditors)

  const updateClassState = () => {
    if ( active ) {
      mainContent.classList.add('editorActive')
    } else {
      mainContent.classList.remove('editorActive')
    }

    if ( open ) {
      mainContent.classList.add('editorOpen')
    } else {
      mainContent.classList.remove('editorOpen')
    }
  }

  const fieldEditors = (obj, self) => {
    const visEditors = visualizers.reduce( (acc, v) =>
      v.editors ? ([...acc, ...v.editors(obj, self)]) : acc, [])

    return obj ? [...obj.editors(), ...visEditors] : visEditors
  }

  const restore = () => (scheduler.reset(), world.load(worldSnaphot))
  const snapshot = () => worldSnaphot = world.save()

  const self = {
    activate: (autoOpen = false) => {
      if ( !active ) {
        active = true
        updateClassState()

        visualizers.forEach( v => v.editorActivated && v.editorActivated(self) )
        restore()
      }

      if ( !open && autoOpen ) {
        self.open()
      }
      
      return true
    },

    deactivate: () => {
      if ( !active ) {
        return false
      }

      active = false
      updateClassState()

      visualizers.forEach( v => v.editorDeactivated && v.editorDeactivated(self) )
      snapshot()
      restore()

      sectionsContainer.focus()
      self.deselect()

      return true
    },

    open: () => {
      if ( !active || open ) {
        return false
      }

      open = true
      updateClassState()

      visualizers.forEach( v => v.editorOpened && v.editorOpened(self) )

      return true
    },

    close: () => {
      if ( !active || !open ) {
        return false
      }

      open = false
      updateClassState()

      visualizers.forEach( v => v.editorOpened && v.editorOpened(self) )

      return true
    },

    deselect: () => {
      sectionsContainer.innerHTML = ''
      editing = null
    },

    toggleOpen: () => (open ? self.close() : self.open()),
    toggleActive: () => (active ? self.deactivate() : self.activate()),

    edit: object => {
      self.activate(object)

      if ( object === editing ) {
        return
      }

      self.deselect()
      
      const desc = fieldEditors(object, self)

      desc.map( ({label, fields}) => {
        const header = document.createElement('div')
        header.className = 'header'
        header.innerText = label

        const section = document.createElement('ul')
        section.className = 'editorSection'

        fields.map( field => {
          const { type, label } = field
          
          const comp = document.createElement('li')
          comp.className = `field ${type}`

          const labelElement = document.createElement('label')
          labelElement.className = `fieldName column`
          labelElement.innerText = label
          comp.appendChild(labelElement)

          const fieldElement = document.createElement('div')

          const builder = fieldEditorMap[type]
          if ( builder ) {
            fieldElement.className = `fieldEditor column ${type}`

            const edit = field.edit
            if ( edit ) {
              // Reset object after editing it if
              // there is no return value.

              field.edit = v => {
                if ( edit(v) !== true ) {
                  object.reset()
                }
              }
            }

            builder(field, fieldElement, comp)
          } else {
            fieldElement.className = `fieldEditor column label`
            fieldElement.innerText = `Unknown field type: ${type}`
          }
          comp.appendChild(fieldElement)

          return comp
        }).forEach( c => section.appendChild(c) )

        return { header, section }
      }).forEach( ({header, section}) => sectionsContainer.append(header, section) )
    }
  }

  eventListenerSet([
    { object: editorBar, event: 'click', listener: event => 
      ( event.preventDefault(), self.toggleOpen()) },
    { object: document, event: 'keydown', listener: event =>
      (event.code === toggleKey) && (event.preventDefault(), self.toggleActive()) }
  ]).setup()

  if ( activate ) {
    self.activate()
  }

  updateClassState()
  return self
}

export default editor
