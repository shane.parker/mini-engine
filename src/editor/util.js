
export const safeParseFloat = (n, d) => {
  const v = Number.parseFloat(n)
  return (Number.isNaN(v) ? d : v)
}
