
const audio = engine => {
  const sounds = {}

  return {
    play: (name, opts = {}) => new Promise( (resolve, reject) => {
      const { loop = false } = opts

      const entry = sounds[name]
      if ( !entry ) {
        engine.log.warning('Invalid sound name:', name)
        reject(false)
        return
      }

      const { path, ready, instances } = entry
      if ( !ready ) {
        engine.log.warning('Sound is not ready for playing:', name)
        reject(false)
        return
      }

      const instance = new Audio(path)
      instance.loop = loop

      instances.add(instance)

      instance.addEventListener('ended', () => {
        engine.log.debug('Sound finished playing:', name)
        instances.delete(instance)
        resolve(name)
      })

      instance.play().catch(reject)
    }),

    sound: (name, path, opts = {}) => {
      const { cb, stream = false } = opts

      if ( sounds[name] ) {
        return false
      }

      const audio = new Audio(path)
      const sound = { audio, name, path, ready: false, instances: new Set() }

      if ( stream !== true ) {
        audio.addEventListener('canplaythrough', () => {
          engine.log.debug('Sound ready to play:', sound.name, sound.path)
          sound.ready = true

          if ( cb ) {
            cb()
          }
        })
        audio.preload = "auto"
        audio.load()
      } else {
        audio.preload = "metadata"
        ready = true
      }
      

      sounds[name] = sound

      return true
    },

    reset: () => Object.keys(sounds).forEach(k => {
      sounds[k].instances.forEach( audio => {
        audio.pause()
        audio.remove()        
      })
      
      delete sounds[k]
    })
  }
}

export default audio
