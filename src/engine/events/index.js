const events = () => {
  const eventListenerMap = {}

  const self = {
    addEventListener: (name, listener) => {
      const list = eventListenerMap[name] ||
        (eventListenerMap[name] = new Set())

      list.add(listener)
    },

    removeEventListener: (name, listener) => {
      const list = eventListenerMap[name]
      if ( list ) {
        list.delete(listener)
      }
    },

    postEvent: async (name, data) => {
      const listeners = eventListenerMap[name] || []
      listeners.forEach( l => {
        try {
          l(data)
        } catch ( err ) {
          logger.error(`Error handling event '${name}':`, err)
        }
      })
    }
  }

  return self
}

export default events
