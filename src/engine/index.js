import { clone, getObjectPath, setObjectPath, dynamicStore } from '@util/object'

import schedulerInit from './scheduler'
import editorInit from '../editor'
import worldInit from './world'
import audioInit from './audio'
import eventsInit from './events'
import log from './log'

const SETTINGS_KEY = 'engine.settings'

const engine = (opts = {}) => {
  const {
    settingsKey = SETTINGS_KEY,
    visualizers: visualizersInitList = [],
    controllers: controllerInitMap = {},
    editorEnabled = true
  } = opts

  const updateHooks = new Set()

  let visualizers = []
  let lastTick = 0

  const logger = log()
  const events = eventsInit()

  const state = {}

  const {
    reset: resetScheduler,
    scheduler
  } = schedulerInit()
  
  let audio = null
  let editor = null
  let world = null      
  
  const loadSettings = () => {
    try {
      const str = localStorage.getItem(settingsKey)
      return str ? JSON.parse(str) : clone({})
    } catch ( err ) {
      logger.error(err)
      return {}
    }
  }

  const settings = dynamicStore({
    root: loadSettings(),
    store: (root, path, value) => {
      setObjectPath(root, path, value)
      localStorage.setItem(settingsKey, JSON.stringify(root))
      return value
    }
  })

  const self = {
    get log() { return logger },

    extend: (name, ext) => {
      if ( self[name] ) {
        throw Error(`Engine extension with name ${name} already exists`)
      }

      self[name] = ext
    },

    init: async () => {
      // Initialize the visualizers
      visualizers = await Promise.all( visualizersInitList.map( v => v(self) ))

      // Set up built in services
      world = worldInit({engine: self, visualizers, controllerInitMap}, opts)
      audio = audioInit(self)

      // Initialize editor if needed
      editor = editorEnabled ? editorInit({
        engine: self,
        visualizers,
        world,
        scheduler: {scheduler, reset: resetScheduler},
      }, opts.editor) : null;

      self.log('Engine initialized')
    },

    get world() { return world },
    get audio() { return audio },
    get editor() { return editor },
    get scheduler() { return scheduler },
    get settings() { return settings },
    get events() { return events },
    get state() { return state },

    hookUpdate: obj => updateHooks.add(obj),
    unhookUpdate: obj => updateHooks.delete(obj),

    update: async t => {
      const delta = (t - (lastTick || t)) / 1000.0
      lastTick = t

      // Trigger tasks before main update
      visualizers.map(v => v.prepare && v.prepare(delta))

      // Update frame hooks
      updateHooks.forEach(h => h.update(delta))

      // Allow each visualizer to update themselves concurrently
      visualizers.map(v => v.update && v.update(delta))

      // Have all visualizers synchronize their state
      await Promise.all(visualizers.map(v => v.sync && v.sync()))
    }
  }

  return self
}

export default engine
