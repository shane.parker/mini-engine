
export const logLevel = {
  DEBUG: 0,
  INFO: 1,
  WARNING: 2,
  ERROR: 3
}

const log = (opts = {}) => {
  let {
    level = 0,
    callback = console.log
  } = opts

  const fn = (...msg) => (level <= logLevel.INFO) ? callback(...msg) : false

  fn.debug = (...msg) => (level <= logLevel.DEBUG) ? callback('[DEBUG]', ...msg) : false
  fn.warning = (...msg) => (level <= logLevel.WARNING) ? callback('[WARNING]', ...msg) : false
  fn.error = (...msg) => (level <= logLevel.ERROR) ? callback('[ERROR]', ...msg) : false
  fn.info = fn

  fn.level = l => level = l

  return fn
}

export default log
