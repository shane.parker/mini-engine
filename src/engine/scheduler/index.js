const scheduler = () => {
  let nextTimeoutId = 0
  let timeouts = {}

  const reset = () => {
    Object.values(timeouts).forEach( v => v.cancel() )
    nextTimeoutId = 0
  }

  const self = {
    wait: (ms, v) => {
      const id = ++nextTimeoutId

      const ctx = {
        timeout: null,
        resolve: null,
        reject: null
      }

      ctx.timeout = setTimeout(() => {
        delete timeouts[id]
        ctx.resolve(v)
      }, ms)

      const promise = new Promise( (resolve, reject) => {
        if ( ctx.timeout === null ) {
          reject()
        }

        ctx.resolve = resolve
        ctx.reject = reject
      })

      promise.cancel = () => {
        clearTimeout(ctx.timeout)
        ctx.timeout = null
        ctx.reject()

        delete timeouts[id]
      }

      timeouts[id] = promise
      return promise
    }
  }

  return {scheduler: self, reset}
}

export default scheduler
