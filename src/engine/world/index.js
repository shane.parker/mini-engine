
import generateObjectID from '@util/id-generator'
import object from './object'

const world = ({
  engine,
  visualizers,
  controllerInitMap
}, opts = {}) => {
  const {
    idGenerator = generateObjectID,
    indexes: indexNames = []
  } = opts;

  const objectMap = {}

  const self = {
    create: (meta = {}, opts = {}) => {
      const { id = idGenerator() } = meta
      const { activate = true } = opts

      if ( objectMap[id] !== undefined ) {
        throw Error(`Attempt to create object with duplicate ID: ${id}`)
      }
      
      engine.log.debug(`Creating object with ID ${id}`)

      return (objectMap[id] = object({ ...meta, id }, {
        self, engine, visualizers, objectMap, activate, controllerInitMap
      }))
    },

    broadcast: (msg, type) =>
      Object.values(objectMap).forEach( obj => {
        if ( !type || (obj.type() === type) ) {
          obj.send(msg)
        }
      }),

    get: id => objectMap[id],

    find: fn => Object.values(objectMap).find(fn),

    map: fn => Object.values(objectMap).map(fn),

    each: fn => Object.values(objectMap).each(fn),

    filter: fn => Object.values(objectMap).filter(fn),

    save: () => 
      ({ objects: Object.values(objectMap).map(o => !o.transient && o.save()) }),

    load: (data = {}, opts = {}) => {
      const { reset = true } = opts
      if ( reset ) {
        Object.values(objectMap).forEach( o => o.destroy() )
      }

      const { objects = [] } = data
      objects.forEach( o => self.create(o) )

      return true
    },

    pick: (pos, opts = {}) => visualizers.reduce( (acc, v) =>
      acc.concat((v.pick && v.pick(pos, opts)) || []), [] ),
  }

  return self
}

export default world

