import { valueResolver } from '@util/object'
import vector3 from '@util/vector3'

const object = (data, {
  engine,
  world,
  objectMap,
  visualizers,
  controllerInitMap,
  activate
}) => {
  let {
    id,
    name = '',
    position = vector3(),
    rotation = {x: 0.0, y: 0.0, z: 0.0, w: 1.0},
    ...props
  } = data

  let alive = true
  let active = false
  let activeVisualizers = null
  let controllers = null

  const self = {
    get id() { return id },
    get alive() { return alive },
    get active() { return active },

    set name(n) { return name = n },
    get name() { return name },

    get engine() { return engine },
    get world() { return world },

    activate: () => {
      if ( active ) {
        return
      }

      activeVisualizers = visualizers.filter( v => !v.notify || v.notify(self) )
      activeVisualizers.forEach( v => v.activated(self, engine) )

      const {
        controllers: controllerNames = ((typeof props.controller === 'string') ?
          [props.controller] : (props.controllers || [])),
      } = data

      const controllerInitList = controllerNames
        .map(c => controllerInitMap[c] || engine.log.warning(`No controller found for name ${c}`))
        .filter(e => e !== undefined)

      controllers = controllerInitList.map( c => {
        const cprops = props[c.name] || (props[c.name] = {})
        if ( c.defaultProps ) {
          Object.entries(c.defaultProps).reduce( (acc, [k, v]) => {
            const val = acc[k]
            if ( val === undefined ) {
              acc[k] = (typeof v === 'function') ? v() : v
            }
          }, cprops)
        }

        return c({object: self, resolve: valueResolver(cprops), props: cprops, engine})
      })

      // Notify all controllers that activation is completed
      controllers.forEach( c => c.activated && c.activated() )
      active = true
    },

    deactivate: () => {
      if ( !active ) {
        return
      }

      activeVisualizers.forEach( v => v.deactivated && v.deactivated(self, engine) )
      activeVisualizers = []

      controllers.forEach( c => c.deactivated && c.deactivated(self, engine) )
      controllers = []

      active = false
    },

    reset: () => (self.deactivate(), self.activate()),

    extend: (name, desc) => {
      self[name] = desc
    },

    transform: ({ position: npos = position, 
                  rotation: nrot = rotation, 
                }, visFilter) => {
      position = npos
      rotation = nrot

      visualizers.forEach( v => (v !== visFilter) && v.reposition(self, engine) )
    },

    get props() { return props },
    
    set position(p) { return (self.transform({ position: p }), p) },
    get position() { return position },

    set rotation(r) { return (self.transform({ rotation: r }), r) },
    get rotation() { return rotation },
    
    send: async (name, data) => {
      controllers.forEach( c => {
        const handler = c[name]
        if ( handler ) {
          handler(data)
        } else if ( c._unknown_ ) {
          c._unknown_(name, data)
        }
      })
    },
    
    destroy: () => {
      if ( !alive ) {
        return
      }

      engine.log.debug(`Destroying object ${id}`)

      visualizers.forEach(v => v.destroyed && v.destroyed(self, engine))

      self.deactivate()
      alive = false

      delete objectMap[id]
    },

    editors: () => {
      const result = [{
        label: 'Object',
        fields: [
          { label: 'Object ID', type: 'label', value: self.id },
          { label: 'Name', type: 'string', value: self.name,
            edit: v => (self.name = v, true) },
        ]
      }, {
        label: 'Transform',
        fields: [
          { label: 'Position', type: 'vector3', value: self.position,
            edit: v => (self.position = v, true) },
          { label: 'Rotation', type: 'rotation', value: self.rotation,
            edit: v => (self.rotation = v, true) }
        ]
      }]
      
      return controllers.reduce( (acc, c) =>
        (c.editors && [ ...acc, ...c.editors(self) ]) || acc, result)
    },
    
    save: () => ({
      ...props,
      id,
      name,
      position,
      rotation
    })
  }

  if ( activate ) {
    self.activate()
  }

  return self
}

export default object
