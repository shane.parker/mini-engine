
const controller = (obj, engine) => ({
  message: msg => {
    if ( msg === 'click' ) {
      engine.log(`Object ${obj.id} got message:`, msg)
      obj.playAnimation('animation_AnimatedCube')
      engine.audio.play('bing')
    }
  }
})

export default controller
