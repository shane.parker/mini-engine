import vector3 from '@util/vector3'
import eventListenerSet from '@util/event-listener-set'

const playerController = ({obj, props, engine}) => {
  let {
    heading = vector3(),
    velocity = vector3(),
    acceleration = 1.75,
    decceleration = acceleration * 2,
    moveDelta = 0.0,
    speed = 15.0
  } = props

  const directions = {
    'KeyW': { v: vector3(0.0, 0.0, -1.0) },
    'KeyS': { v: vector3(0.0, 0.0, 1.0) },
    'KeyA': { v: vector3(-1.0, 0.0, 0.0) },
    'KeyD': { v: vector3(1.0, 0.0, 0.0) }
  }

  const directionList = Object.values(directions)
  let moving = false

  const updateHeading = event => {
    heading = vector3.normalize(directionList.reduce( (acc, dir) => 
      dir.active ? vector3.add(acc, dir.v) : acc, vector3()))

    // Clamp to max speed
    heading = vector3.setMagnitude(heading, speed)
  }

  const updateInput = event => {
    if ( event.repeat ) {
      return
    }

    const dir = directions[event.code]
    if ( !dir ) {
      return
    }

    dir.active = (event.type === 'keydown') ? true : false
    updateHeading()
  }

  const keyEvents = eventListenerSet(document, [
    { events: ['keydown', 'keyup'], listener: updateInput  }
  ]);

  updateHeading()

  const self = {
    update: delta => {
      const isHeading = (heading.x !== 0.0) ||
        (heading.y !== 0.0) ||
        (heading.z !== 0.0)

      const isMoving = (velocity.x !== 0.0) ||
        (velocity.y !== 0.0) ||
        (velocity.z !== 0.0)

      if ( isMoving !== moving ) {
        obj.send(isMoving ? 'moveStart' : 'moveStop')
        moving = isMoving
      }

      moveDelta = isHeading ? Math.min(1.0, moveDelta + (delta * acceleration))
        : Math.max(0.0, moveDelta - (delta * decceleration))

      velocity = vector3.scale(vector3.add(velocity, heading), moveDelta)
      if ( vector3.magnitude2(velocity) > (speed*speed) ) {
        velocity = vector3.setMagnitude(velocity, speed)
      }

      //obj.physics.setVelocity({
      //  linear: velocity
      //})
    },

    deactivated: () => {
      engine.unhookUpdate(self)
      keyEvents.cleanup()
    }
  }

  engine.hookUpdate(self)

  return self
}

export default playerController
