
const trigger = ({resolve, engine}) => {
  const { enabled } = resolve()

  const self = {
    triggerEnter: object => enabled() && engine.log.debug('Object entered trigger:', object.id),
    triggerLeave: object => enabled() && engine.debug.log('Object left trigger:', object.id),  

    editors: () => [{
      label: 'Trigger',
      fields: [
        { label: 'Enabled', type: 'boolean', value: enabled(),
          edit: v => (enabled(v), true) }
      ]
    }]
  }

  return self
}

trigger.defaultProps = {
  enabled: true
}

export default trigger
