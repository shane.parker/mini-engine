import engine from '../engine'

import graphicsVisualizer from './visualizers/threejs-visualizer'
import physicsVisualizer from './visualizers/ammo-visualizer'

import triggerController from './controllers/test/trigger'
import playerController from './controllers/test/player'
import ballController from './controllers/test/ball'

import { randomRange } from '@util/random'

const deg2rad = n => n * 0.0174533

const setup = async () => {
  const game = await engine({
    visualizers: [
      graphicsVisualizer({
        selector: '#scene',
        bgColor: 0x34baeb,
        shadows: true
      }),
      physicsVisualizer({
        gravity: { x: 0.0, y: -50.0, z: 0.0 }
      })
    ],
    controllers: {
      ball: ballController,
      trigger: triggerController,
      player: playerController
    },
    editor: {
      activate: true
    }
  })

  await game.init()
  
  document.querySelector('#scene').classList.add('show')

  /*game.audio.sound('bing', 'assets/bing.wav', {
    cb: () => game.log('ready')
  })*/

  game.log.level(0)

  game.world.create({
    id: 'camera',
    visual: { type: 'camera', main: true, controls: true },
    position: { x: 0.0, y: 10.0, z: 50.0 }
  })

  game.world.create({
    id: 'ambientLight',
    visual: { type: 'light', ambient: true, color: 0x101010 },
    position: { x: 0.0, y: 50.0, z: 50.0 }
  })

  game.world.create({
    id: 'light',
    visual: {
      type: 'light', 
      color: 0xffffff,
      intensity: 2.0,
      helper: 5.0,
      castShadow: true
    },
    position: { x: 0.0, y: 50.0, z: 50.0 }
  })

  game.world.create({
    id: 'floor',
    visual: {
      type: 'box',
      width: 100.0,
      height: 1.0,
      depth: 100.0,
      receiveShadow: true
    },
    physics: {
      shape: 'box',
      width: 100.0,
      height: 1.0,
      depth: 100.0,
      restitution: 0.25
    },
    position: { x: 0.0, y: 0.0, z: 0.0 }
  })

  const trigger = game.world.create({
    id: 'trigger',
    visual: {
      type: 'box',
      width: 25.0,
      height: 50.0,
      depth: 25.0,
      receiveShadow: true,
      color: 0xff0000
    },
    physics: {
      shape: 'box',
      width: 25.0,
      height: 50.0,
      depth: 25.0,
      restitution: 0.25,
      trigger: true
    },
    position: { x: -25.0, y: 25.0, z: -30.0 },
    controller: 'trigger'
  })

  game.world.create({
    id: 'player',
    visual: {
      type: 'cylinder',
      height: 15.0,
      radius: 5.0,
      castShadow: true,
      color: '#0000ff'
    },
    physics: {
      shape: 'cylinder',
      height: 15.0,
      radius: 5.0,
      character: {}
    },
    position: { x: 0.0, y: 35.0, z: -0.0 },
    controller: 'player'
  })

  const spawnBall = () => {
    game.world.create({
      visual: {
        type: 'sphere',
        radius: 3.0,
        castShadow: true,
        helper: 10
      },
      physics: {
        shape: 'sphere',
        mass: 3.0,
        restitution: 0.6,
        radius: 3.0
      },
      position: { x: randomRange(40, -40), y: randomRange(40.0 , 80.0), z: randomRange(40, -40) },
      controller: 'ball'
    })
  }

  spawnBall()

  const tick = async time => {
    await game.update(time)
    requestAnimationFrame(tick)
  }

  requestAnimationFrame(tick)
}

export default setup
