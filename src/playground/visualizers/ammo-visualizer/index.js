import SimWorker from './simulator.worker'

import snapshot from './simulation-snapshot'
import vector3 from '@util/vector3'

import protocol, { opcodes } from './protocol'

import { TriggerEvent } from './simulation-snapshot'

const BodyId = Symbol('Simulator rigid body ID')

const META_KEY = 'physics'

const ammoVisualizer = (opts = {}) => engine => new Promise( resolve => {
  const { maxBodies = 512 } = opts
  let { paused = false } = opts

  const idToObject = {}

  let syncBuffer = new ArrayBuffer(2)

  let updatePromise
  let updateResolve
  let bodyCount = 0
  let nextID = 0

  const worker = new SimWorker()

  const syncBodyTransform = (id, pos, rot) => {
    const obj = idToObject[id]
    if ( !obj ) {
      engine.log.error('Transform sync error! Physics body does not exist for ID:', id)
      return
    }

    obj.transform({
      position: pos,
      rotation: rot
    }, self)
  }

  const syncContact = (id1, id2, point1, point2, distance) => {
    // Read id of bodies
    const object1 = idToObject[id1]
    const object2 = idToObject[id2]

    // Bodies may have been removed during previous
    // frame processing. Check for this!
    if ( !object1 || !object2 ) {
      return
    }

    // Notify first object
    object1.send('contact', {
      object: object2,
      point1: point2,
      point2: point1,
      distance
    })

    // Notify second object
    object2.send('contact', {
      object: object1,
      point1,
      point2,
      distance
    })
  }

  const syncTriggerEvent = (event, triggerId, objectId) => {
    const trigger = idToObject[triggerId]
    const object = idToObject[objectId]

    if ( !trigger || !object ) {
      engine.log.warning('Trigger event(', event, ') with unknown object:', trigger, object)
      return
    }

    engine.log.debug('Object', object.id, (event === TriggerEvent.ENTER ? 'entered' : 'left'),
        'trigger', trigger.id)
    
    trigger.send((event === TriggerEvent.ENTER) ? 'triggerEnter' : 'triggerLeave', object)
  }

  const self = {
    notify: obj => (obj.props.physics !== undefined),

    activated: async obj => {
      if ( bodyCount >= maxBodies ) {
        throw Error(`Maximum physics body count reached: ${maxBodies}`)
      }

      const id = ++nextID

      obj.extend('physics', {
        impulse: (impulse, origin = vector3()) =>
          worker.postMessage(protocol.impulse(id, impulse, origin)),

        setVelocity: ({linear, angular}) =>
          worker.postMessage(protocol.config(id, {
            linearVelocity: linear,
            angularVelocity: angular
          })),
          
        setDamping: ({linear, angular}) =>
          worker.postMessage(protocol.config(id, {
            linearDamping: linear,
            angularDamping: angular
          }))
      })

      worker.postMessage(protocol.body(id, obj.props.physics, {
        position: obj.position,
        rotation: obj.rotation
      }))

      obj[BodyId] = id

      idToObject[id] = obj
      bodyCount++
    },

    deactivated: obj => {
      const simId = obj[BodyId]
      if ( !simId ) {
        engine.log.warning('Attempt to remove unknown phyics object:', simId)
        return
      }

      engine.log.debug('Removing simulation body', simId, 'from object', obj.id)
      worker.postMessage(protocol.delete(simId))
        
      delete idToObject[simId]
      bodyCount--
    },

    reposition: obj => {
      const simId = obj[BodyId]
      if ( !simId ) {
        engine.log.warning('Attempt to reposition unknown phyics object:', simId)
        return
      }

      worker.postMessage(protocol.transform(simId, {
        position: obj.position,
        rotation: obj.rotation
      }))
    },

    prepare: delta => {
      if ( paused ) {
        return
      }
      
      worker.postMessage(protocol.simulate(delta, syncBuffer), [syncBuffer])
      updatePromise = new Promise( resolve => {
        updateResolve = resolve
      })
    },

    sync: async () => {
      if ( paused ) {
        return
      }

      // Wait for simulator worker to complete frame
      await updatePromise
      updatePromise = null

      // Synchronize objects with simulation results
      snapshot.deserialize({
        buffer: syncBuffer,
        transformCb: syncBodyTransform,
        characterCb: syncBodyTransform,
        contactCb: syncContact,
        triggerCb: syncTriggerEvent
      })
    },

    editorActivated: editor => {
      paused = true
    },

    editorDeactivated: () => {
      paused = false
    },
  }

  const messageDispatcher = {
    [opcodes.SYNC]: ({ buffer }) => {
      syncBuffer = buffer

      updateResolve(true)
      updateResolve = null
    },

    [opcodes.READY]: () => {
      engine.log('Simulator worker is ready')
      resolve(self)
    }
  }

  worker.onmessage = ({ data }) => messageDispatcher[data.op](data)
  worker.onerror = (msg, filename, line) => engine.log.error(filename, line, msg)

  // Start the simulation worker
  worker.postMessage(protocol.init(opts))

  return self
})

export default ammoVisualizer
