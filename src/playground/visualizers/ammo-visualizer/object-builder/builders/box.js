
import { toBtVector3 } from '../../util'

const builder = ammo => ({width, height, depth}) =>
  new ammo.btBoxShape(toBtVector3(ammo, {
    x: width / 2.0,
    y: height / 2.0,
    z: depth / 2.0
  }))

export default builder
