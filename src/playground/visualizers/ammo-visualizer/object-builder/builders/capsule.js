
const builder = ammo => ({ radius = 1.0, height = 1.0 }) =>
  new ammo.btCapsuleShape(radius, height)

export default builder
