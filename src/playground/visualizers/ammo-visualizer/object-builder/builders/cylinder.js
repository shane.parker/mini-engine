
import { toBtVector3 } from '../../util'

const builder = ammo => ({ radius = 1.0, height = 1.0 }) =>
  new ammo.btCylinderShape(toBtVector3(ammo, { 
    x: radius, y: height / 2.0, z: radius
  }))

export default builder
