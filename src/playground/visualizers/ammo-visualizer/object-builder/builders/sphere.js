
const builder = ammo => ({ radius = 1.0 }) => 
  new ammo.btSphereShape(radius)

export default builder
