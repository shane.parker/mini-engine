import cylinderbuilder from './builders/cylinder'
import capsuleBuilder from './builders/capsule'
import sphereBuilder from './builders/sphere'
import boxBuilder from './builders/box'

import vector3 from '../../../../util/vector3'

const shapeBuilder = (log, ammo) => {
  const builders = {
    'cylinder': cylinderbuilder(ammo),
    'capsule': capsuleBuilder(ammo),
    'sphere': sphereBuilder(ammo),
    'box': boxBuilder(ammo)
  }

  const prepare = (shape, desc) => {
    const {
      mass = 0.0,
      friction = 0.5,
      restitution = 0.0,
      character,
      trigger = false
    } = desc
  
    if ( trigger === true ) {
      const obj = new ammo.btPairCachingGhostObject()
      obj.setCollisionShape(shape)
  
      // Disable contact response for triggers
      // (Flag value CF_NO_CONTACT_RESPONSE)
      obj.setCollisionFlags(obj.getCollisionFlags() | 4)
      
      return obj
    } else if ( character ) {
      const { stepHeight = 0.25, up = vector3(0.0, 1.0, 0.0) } = character

      const ghost = new ammo.btPairCachingGhostObject()
      ghost.setCollisionShape(shape)
      ghost.setCollisionFlags(ghost.getCollisionFlags() | 16)

      const obj = new ammo.btKinematicCharacterController(ghost, shape, stepHeight, up)
      obj.setLinearVelocity = v => {
        obj.setVelocityForTimeInterval(v, 1.0)
      }

      return obj
    }
  
    const localInertia = new ammo.btVector3(0, 0, 0)
    shape.calculateLocalInertia(mass, localInertia)
  
    const bodyInfo = new ammo.btRigidBodyConstructionInfo(mass, null, shape, localInertia)
    bodyInfo.m_friction = friction
    bodyInfo.m_restitution = restitution

    return new ammo.btRigidBody(bodyInfo)
  }
  
  const builder = (desc, transform) => {
    const shapeBuilder = builders[desc.shape]
    if ( !shapeBuilder ) {
      log('Invalid shape type:', desc.shape)
      return
    }

    return prepare(shapeBuilder(desc), desc, transform)
  }

  return builder
}

export default shapeBuilder
