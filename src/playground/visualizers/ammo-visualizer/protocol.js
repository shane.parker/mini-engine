export const opcodes = {
  NOP: 0,
  INIT: 1,
  READY: 2,
  BODY: 3,
  DELETE: 4,
  SIMULATE: 5,
  SYNC: 6,
  TRANSFORM: 7,
  IMPULSE: 8,
  CONFIG: 9,
  PAUSE: 10
}

export default {
  init: opts => ({ op: opcodes.INIT, opts }),
  ready: () => ({ op: opcodes.READY }),
  body: (id, desc, transform) => ({ op: opcodes.BODY, id, desc, transform }),
  delete: id => ({ op: opcodes.DELETE, id }),
  simulate: (delta, buffer) => ({ op: opcodes.SIMULATE, delta, buffer }),
  sync: buffer => ({ op: opcodes.SYNC, buffer }),
  transform: (id, transform) => ({ op: opcodes.TRANSFORM, id, transform }),
  impulse: (id, impulse, origin) => ({ op: opcodes.IMPULSE, id, impulse, origin }),
  config: (id, config) => ({ op: opcodes.CONFIG, id, config }),
  pause: (id, paused) => ({ op: opcodes.PAUSE, paused })
}
