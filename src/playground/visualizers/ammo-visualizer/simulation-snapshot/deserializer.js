
import bufferStream from '@util/buffer-stream'
import vector3 from '@util/vector3'

const syncBodyTransforms = (stream, cb) => {
  // Read rigid body transforms
  const rcount = stream.readUint32()

  for ( let index = 0; index < rcount; index++ ) {
    // Read ID
    const id = stream.readUint32()

    // Read position
    const px = stream.readFloat32()
    const py = stream.readFloat32()
    const pz = stream.readFloat32()

    // Read rotation
    const rx = stream.readFloat32()
    const ry = stream.readFloat32()
    const rz = stream.readFloat32()
    const rw = stream.readFloat32()

    cb(id, { x: px, y: py, z: pz }, { x: rx, y: ry, z: rz, w: rw })
  }
}

const syncCharacterTransforms = (stream, cb) => {
  // Read character transforms
  const ccount = stream.readUint32()

  for ( let index = 0; index < ccount; index++ ) {
    // Read ID
    const id = stream.readUint32()

    // Read position
    const px = stream.readFloat32()
    const py = stream.readFloat32()
    const pz = stream.readFloat32()

    cb(id, {x: px, y: py, z: pz})
  }
}

const syncContacts = (stream, cb) => {
  // Read contact count
  const contactCount = stream.readUint32()

  for ( let index = 0; index < contactCount; index++ ) {
    // Read id of bodies
    const id1 = stream.readUint32()
    const id2 = stream.readUint32()

    // Read point on first body
    const px1 = stream.readFloat32()
    const py1 = stream.readFloat32()
    const pz1 = stream.readFloat32()

    // Read point on second body
    const px2 = stream.readFloat32()
    const py2 = stream.readFloat32()
    const pz2 = stream.readFloat32()

    const point1 = vector3(px1, py1, pz1)
    const point2 = vector3(px2, py2, pz2)

    // Read intersection distance
    const distance = stream.readFloat32()

    cb(id1, id2, point1, point2, distance)
  }
}

const syncTriggerEvents = (stream, cb) => {
  // Read trigger event count
  const triggerEventCount = stream.readUint32()

  for ( let index = 0; index < triggerEventCount; index++ ) {
    const event = stream.readUint32()
    const triggerId = stream.readUint32()
    const objectId = stream.readUint32()

    cb(event, triggerId, objectId)
  }
}

export const deserializer = ({ buffer, transformCb, characterCb, contactCb, triggerCb }) => {
  const stream = bufferStream(buffer)

  syncBodyTransforms(stream, transformCb)
  syncCharacterTransforms(stream, characterCb)
  syncContacts(stream, contactCb)
  syncTriggerEvents(stream, triggerCb)
}

export default deserializer
