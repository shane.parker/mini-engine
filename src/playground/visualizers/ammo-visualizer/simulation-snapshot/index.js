
import serializer from './serializer'
import deserializer from './deserializer'

import { TriggerEvent as triggerEventsEnum } from './constants'

export const TriggerEvent = triggerEventsEnum

export default {
  serialize: serializer,
  deserialize: deserializer,
}
