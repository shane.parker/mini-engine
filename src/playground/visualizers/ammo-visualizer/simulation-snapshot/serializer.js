
import bufferStream from '@util/buffer-stream'
import { TriggerEvent } from './constants'

const insideTrigger = (trigger, id) => {
  const count = trigger.getNumOverlappingObjects()
  for ( let x = 0; x < count; x++ ) {
    if ( trigger.getOverlappingObject(x).getUserIndex() === id ) {
      return true;
    }
  }

  return false
}

export const serializer = ({ world, bodies, characters, triggers, buffer, symbols }) => {
  const writeTransforms = stream => {
    const blist = Object.values(bodies)

    // Write rigid body transforms
    stream.reserve(4).writeUint32(blist.length)
  
    blist.forEach( body => {
      const syncTransform = body.getWorldTransform()
      const origin = syncTransform.getOrigin()
      const rotation = syncTransform.getRotation()
  
      // Write ID
      stream.reserve(4).writeUint32(body.getUserIndex())
  
      // Write position
      stream.reserve(12)
        .writeFloat32(origin.x())
        .writeFloat32(origin.y())
        .writeFloat32(origin.z())
  
      // Write rotation
      stream.reserve(16)
        .writeFloat32(rotation.x())
        .writeFloat32(rotation.y())
        .writeFloat32(rotation.z())
        .writeFloat32(rotation.w())
    })
  }

  const writeCharacters = (stream, symbols) => {
    // Write character transforms
    const clist = Object.values(characters)
    stream.reserve(4).writeUint32(clist.length)
  
    clist.forEach( character => {
      const pos = character.getGhostObject()
        .getWorldTransform().getOrigin()
  
      // Write ID
      stream.reserve(4).writeUint32(character[symbols.CharacterSymbol])
      
      // Write position
      stream.reserve(12)
        .writeFloat32(pos.x())
        .writeFloat32(pos.y())
        .writeFloat32(pos.z())
    })
  }
  
  const writeContacts = stream => {
    const collisionDispatcher = world.getDispatcher()
    const contactCount = collisionDispatcher.getNumManifolds()
  
    stream.reserve(4).writeUint32(contactCount)
  
    for ( let x = 0; x < contactCount; x++ ) {
      const contact = collisionDispatcher.getManifoldByIndexInternal(x)
  
      const b1 = contact.getBody0().getUserIndex()
      const b2 = contact.getBody1().getUserIndex()
  
      const pt = contact.getContactPoint()
      const pa = pt.getPositionWorldOnA()
      const pb = pt.getPositionWorldOnB()
  
      // Write the body IDs
      stream.reserve(4).writeUint32(b1).writeUint32(b2)
  
      // Write point on body A
      stream.reserve(12)
        .writeFloat32(pa.x())
        .writeFloat32(pa.y())
        .writeFloat32(pa.z())
  
      // Write point on body B
      stream.reserve(12)
        .writeFloat32(pb.x())
        .writeFloat32(pb.y())
        .writeFloat32(pb.z())
  
      // Write the intersection distance
      stream.reserve(4).writeFloat32(pt.getDistance())
    }
  }

  const writeTriggerEvents = stream => {
    const events = []

    // Process trigger events
    triggers.forEach( trigger => { 
      const { object, overlapped } = trigger

      const count = object.getNumOverlappingObjects()
      const triggerId = object.getUserIndex()
      
      // Find objects that are newly entered
      for ( let x = 0; x < count; x++ ) {
        const o = object.getOverlappingObject(x)
        const objectId = o.getUserIndex()
        
        if ( !overlapped.has(objectId) ) {
          events.push({
            event: TriggerEvent.ENTER,
            triggerId,
            objectId
          })

          overlapped.add(objectId)
        }
      }

      // Find objects that have left the trigger
      overlapped.forEach( objectId => {
        if ( !insideTrigger(object, objectId) ) {
          events.push({
            event: TriggerEvent.LEAVE,
            triggerId,
            objectId
          })

          overlapped.delete(objectId)
        }
      })
    })

    // Write the events to the stream
    stream.reserve(4).writeUint32(events.length)

    events.forEach( ({ event, triggerId, objectId }) => {
      stream.reserve(12)
        .writeUint32(event)
        .writeUint32(triggerId)
        .writeUint32(objectId)
    })
  }

  // Write entire snapshot to the buffer
  const stream = bufferStream(buffer)

  writeTransforms(stream)
  writeCharacters(stream, symbols)
  writeContacts(stream)
  writeTriggerEvents(stream)

  // Return buffer (Which may have been resized)
  return stream.buffer()
}

export default serializer
