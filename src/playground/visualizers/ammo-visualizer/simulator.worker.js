import Ammo from './ammo/ammo'

import objectBuilder from './object-builder'
import { toBtVector3, toBtQuaternion } from './util'

import vector3 from '@util/vector3'

import snapshot from './simulation-snapshot'
import protocol, { opcodes } from './protocol'

const CharacterSymbol = Symbol('Character ID')

const triggers = new Set()
const characters = {}
const bodies = {}

let bodyCount = 0
let builder
let ammo
let world

const log = (...msg) => console.log('[SIMULATOR]', ...msg)

const configureObject = (ammo, object, config) => {
  const {
    linearVelocity,
    angularVelocity,
    linearDamping,
    angularDamping,
    maxSlope,
    gravity,
    mass
  } = config

  if ( (linearVelocity !== undefined) && object.setLinearVelocity ) {
    object.setLinearVelocity(toBtVector3(ammo, linearVelocity))
  }

  if ( (angularVelocity !== undefined) && object.setAngularVelocity ) {
    object.setAngularVelocity(toBtVector3(ammo, angularVelocity))
  }

  if ( (linearDamping !== undefined) && object.setLinearDamping ) {
    object.setLinearDamping(linearDamping)
  }

  if ( (angularDamping !== undefined) && object.setAngularDamping ) {
    object.setAngularDamping(angularDamping)
  }

  if ( (gravity !== undefined) && object.setGravity ) {
    object.setGravity(toBtVector3(ammo, gravity))
  }

  if ( (mass !== undefined) && object.setMassProps ) {
    const localInertia = new ammo.btVector3(0, 0, 0)
    object.getShape().calculateLocalInertia(mass, localInertia)
    object.setMassProps(mass, localInertia)
  }

  if ( (maxSlope !== undefined) && object.setMaxSlope ) {
    object.setMaxSlope(maxSlope)
  }
}

const messageDispatcher = {
  [opcodes.SIMULATE]: ({ delta, buffer }) => {
    world.stepSimulation(delta, 2)

    const nbuffer = snapshot.serialize({
      buffer, world, bodies, 
      characters, triggers,
      symbols: { CharacterSymbol }
    })
    
    self.postMessage(protocol.sync(nbuffer), [nbuffer])
  },

  [opcodes.INIT]: async ({ opts }) => {
    log('Simulator worker initializing')
    await init(opts)
    self.postMessage(protocol.ready())
  },

  [opcodes.BODY]: ({ id, desc, transform }) => {
    const object = builder(desc)
    if ( !object ) {
      log('Cannot build shape:', desc)
      return
    }

    const { collisionMask = -1, trigger, character } = desc

    if ( character ) {
      object[CharacterSymbol] = id
      characters[id] = object

      object.warp(toBtVector3(ammo, transform.position))
      object.setGravity(world.getGravity())

      const ghost = object.getGhostObject()
      ghost.setUserIndex(id)

      world.addCollisionObject(object.getGhostObject())
      world.addAction(object)

      log('Added character with id:', id)
      return
    }
    
    const startTransform = new ammo.btTransform();
    startTransform.setIdentity()
    startTransform.setOrigin(toBtVector3(ammo, transform.position))
    startTransform.setRotation(toBtQuaternion(ammo, transform.rotation))

    object.setWorldTransform(startTransform)
    object.setUserIndex(id)

    if ( trigger === true ) {
      world.addCollisionObject(object, 1, collisionMask)
      triggers.add({object, overlapped: new Set()})
      log('Added collision object with ID', id, ':', desc)
    } else {
      world.addRigidBody(object, 1, collisionMask)
      log('Added rigid body with ID', id, ':', desc)
    }

    bodies[id] = object

    bodyCount++
  },

  [opcodes.DELETE]: ({ id }) => {
    const body = bodies[id]
    if ( body ) {
      world.removeRigidBody(body)
      
      triggers.delete(body)
      delete bodies[id]

      bodyCount--
      return
    }

    const character = characters[id]
    if ( character ) {
      world.removeCollisionObject(character.getGhostObject())
      world.removeAction(character)
      delete characters[id]
    }
  },

  [opcodes.TRANSFORM]: ({id, transform}) => {
    const body = bodies[id]
    if ( body ) {
      const newTransform = new ammo.btTransform();
      newTransform.setIdentity();
      newTransform.setOrigin(toBtVector3(ammo, transform.position))
      newTransform.setRotation(toBtQuaternion(ammo, transform.rotation))
    
      body.setWorldTransform(newTransform)
      return
    }

    const character = characters[id]
    if ( character ) {
      character.warp(toBtVector3(ammo, transform.position))
      return
    }

    log('Attempt to update transform for unknown physics object:', id)
  },

  [opcodes.CONFIG]: ({id, config}) => {
    const object = bodies[id] || characters[id]
    if ( !object ) {
      log('Attempt to configure unknown physics object:', id)
      return
    }

    configureObject(ammo, object, config)
  },

  [opcodes.IMPULSE]: ({id, impulse, origin}) => {
    const body = bodies[id]
    if ( body ) {
      body.applyImpulse(toBtVector3(ammo, impulse), toBtVector3(ammo, origin))
    }
  }
}

self.onmessage = async ({ data }) => messageDispatcher[data.op](data)

const loadAmmo = () => new Promise( resolve => {
  Ammo().then( a => (ammo = a, resolve(true)) )
})

const init = async opts => {
  const {
    gravity = vector3(0.0, -10.0, 0.0)
  } = opts

  await loadAmmo()

  log('Ammo physics intialized')

  builder = objectBuilder(log, ammo)

  const collisionConfiguration = new ammo.btDefaultCollisionConfiguration()
  const collisionDispatcher = new ammo.btCollisionDispatcher(collisionConfiguration)
  const solver = new ammo.btSequentialImpulseConstraintSolver()
  const overlappingPairCache = new ammo.btDbvtBroadphase()

  world = new ammo.btDiscreteDynamicsWorld(collisionDispatcher, overlappingPairCache, solver, collisionConfiguration)
  world.getBroadphase().getOverlappingPairCache().setInternalGhostPairCallback(new ammo.btGhostPairCallback())
  world.setGravity(toBtVector3(ammo, gravity))

  return true
}
