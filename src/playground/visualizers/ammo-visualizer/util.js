export const toBtVector3 = (a, {x, y, z}) => new a.btVector3(x, y, z)

export const toBtQuaternion = (a, {x, y, z, w}) => {
  if ( w === undefined ) {
    const tmp = new a.btQuaternion()
    tmp.setEulerZYX(z, y, x)
    return tmp
  }

  return new a.btQuaternion(x, y, z, w)
}
