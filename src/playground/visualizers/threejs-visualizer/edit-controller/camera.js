import eventListenerSet from '@util/event-listener-set'
import mousePosition from '@util/mouse-position'
import vector3 from '@util/vector3'
import rotation from '@util/rotation'

const BUTTON_PICK = 0

const HALF_PI = 3.14159/2.0

const editCamera = ({
  engine,
  editController,
  visualizer
 }) => {
  const directions = {
    'KeyW': { v: vector3(0.0, 0.0, -1.0) },
    'KeyS': { v: vector3(0.0, 0.0, 1.0) },
    'KeyA': { v: vector3(-1.0, 0.0, 0.0) },
    'KeyD': { v: vector3(1.0, 0.0, 0.0) }
  }

  const directionList = Object.values(directions)
  const container = engine.state.visual.container

  let {
    position = vector3(0.0, 20.0, 20.0),
    rotX = 0.0, rotY = 0.0,
    speed = 150.0
  } = engine.settings.editor.camera()

  const saveCameraSettings = () =>
    engine.settings.editor.camera({
      position,
      rotX, rotY,
      speed
    })

  // Restore position/rotation of camera
  engine.state.visual.camera.position = position
  engine.state.visual.camera.rotate(vector3(rotY, rotX))

  let openedContextMenu = false
  let mouseDragged = false

  const calculateHeading = () => {
    const heading = vector3.normalize(directionList.reduce( (acc, dir) => 
      dir.active ? vector3.add(acc, dir.v) : acc, vector3()))

    return engine.state.visual.camera.rotateVector(heading)
  }

  const updateKeyInput = event => {
    event.preventDefault()
    if ( event.repeat ) {
      return
    }

    const dir = directions[event.code]
    if ( !dir ) {
      return
    }

    dir.active = (event.type === 'keydown') ? true : false

    if ( event.type === 'keyup' ) {
      saveCameraSettings()
    }
  }

  const objectAtEvent = event =>
    visualizer.pick(mousePosition(container, event))[0]

  const mouseMove = event => {
    event.preventDefault()
    container.classList.remove('editorPointer')

    if ( (document.activeElement === container)
      && (event.buttons > 0) && !mouseDragged ) {
      editController.closeContextMenu()
      mouseDragged = true
    }

    if ( mouseDragged ) {
      rotX = rotX + (event.movementX * -0.01)
      rotY = Math.max(-HALF_PI, Math.min(rotY + (event.movementY * -0.01), HALF_PI))
  
      engine.state.visual.camera.rotate(vector3(rotY, rotX))
    }

    const obj = objectAtEvent(event)
    if ( obj  ) {
      container.classList.add('editorPointer')
    }
  }

  const click = event => {
    event.preventDefault()
  }

  const mouseDown = event => {
    event.preventDefault()

    container.focus()
    container.requestPointerLock()

    mouseDragged = false
  }

  const contextMenu = event => {
    event.preventDefault()

    const obj = objectAtEvent(event)
    if ( obj ) {
      editController.showContextMenu(event, {
        title: obj.id,
        actions: [
          { label: 'Edit', action: () => editController.select(obj) },
          { label: 'Delete', action: () => obj.destroy() }
        ]
      })
      openedContextMenu = true
    }
  }

  const mouseUp = event => {
    if ( document.activeElement !== container ) {
      return
    }

    event.preventDefault()
    document.exitPointerLock()

    if ( openedContextMenu ) {
      openedContextMenu = false
      return
    } else if ( editController.closeContextMenu() ) {
      return
    }

    if ( mouseDragged ) {
      mouseDragged = false
      saveCameraSettings()
      return
    }

    if ( event.button === BUTTON_PICK ) {
      const obj = objectAtEvent(event)
      editController.select(obj)
    }
  }

  // Key event handlers. Activated on focus
  const keyEvents = eventListenerSet(container, [{
    events: ['keydown', 'keyup'],
    listener: updateKeyInput
  }])

  if ( document.activeElement === container ) {
    keyEvents.setup()
  }

  // Cursor event handlers
  const cursorEvents = eventListenerSet(container, [
    //{ object: document, event: 'pointerlockchange', listener: e => console.log(e) },
    //{ object: document, event: 'pointerlockerror', listener: e => console.log(e) },
    { event: 'click', listener: click },
    { event: 'contextmenu', listener: contextMenu },
    { event: 'mousedown', listener: mouseDown },
    { event: 'mousemove', listener: mouseMove },
    { event: 'mouseup', listener: mouseUp },
    { event: 'focus', listener: () => keyEvents.setup() },
    { event: 'blur', listener: () => keyEvents.cleanup() }
  ]).setup()

  const self = {
    update: delta => {
      const move = vector3.setMagnitude(calculateHeading(), speed * delta)
      position = vector3.add(position, move)

      engine.state.visual.camera.position = position
    },

    cleanup: () => {
      engine.unhookUpdate(self)
      cursorEvents.cleanup()
      keyEvents.cleanup()
    }
  }

  engine.hookUpdate(self)

  return self
}

export default editCamera
