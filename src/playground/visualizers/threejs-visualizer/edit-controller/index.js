import editCamera from './camera'

const editController = ({
  engine,
  visualizer,
  visualBuilder,
  container,
  editor
})=> {
  let selected = null
  let contextMenu = null
  let camera = null

  const self = {
    closeContextMenu: () => {
      if ( contextMenu ) {
        container.removeChild(contextMenu)
        contextMenu = null
        return true
      }

      return false
    },
  
    showContextMenu: (event, desc) => {
      self.closeContextMenu()
  
      const { title, actions } = desc;
  
      contextMenu = document.createElement('ul')
      contextMenu.classList.add('editorPopup')
      contextMenu.style.top = `${event.clientY + 5}px`
      contextMenu.style.left = `${event.clientX}px`
  
      const header = document.createElement('li')
      header.className = 'popupHeader'
      header.innerText = title
      contextMenu.appendChild(header)
  
      contextMenu.append(...actions.map( ({label, action}, idx) => {
        const item = document.createElement('li')
        item.className = `popupItem ${idx}`
        item.innerText = label
        item.addEventListener('click', event => {
          event.preventDefault()
          self.closeContextMenu()
          if ( action ) {
            action()
          }
        })
  
        return item
      }))
    
      setTimeout(() => contextMenu.classList.add('show'), 10)
    
      container.appendChild(contextMenu)
    },

    select: obj => {
      if ( obj !== selected ) {
        selected = obj
        editor.edit(obj)
      }
    },

    editors: obj => {
      if ( !obj ) {
        // TODO: Return global settings
        return []
      }

      // Get visual object settings
      const visualProps = obj.props.visual

      const {
        type,
        color,
        castShadow,
        receiveShadow
      } = visualProps

      // Add base visual fields
      const sections = [{
        label: 'Visual',
        fields: [
          { label: 'Type', type: 'select', value: type, options: [
            { label: 'None', value: undefined },
            { label: 'Mesh', value: 'mesh' },
            { label: 'Light', value: 'light' },
            { label: 'Box', value: 'box' },
            { label: 'Sphere', value: 'sphere' },
            { label: 'Cylinder', value: 'cylinder' }
          ], edit: v => ((v !== 'none') ? visualProps.type = v : obj.props.visual = undefined, false) },
          { label: 'Color', type: 'color', value: color,
            edit: v => (visualProps.color = v, false) },
          { label: 'Cast Shadow', type: 'boolean', value: castShadow,
            edit: v => (visualProps.castShadow = v, false) },
          { label: 'Receive Shadow', type: 'boolean', value: receiveShadow,
            edit: v => (visualProps.receiveShadow = v, false) }
        ]
      }]

      const visSections = visualBuilder.editors(obj, type, visualProps)
      return [...sections, ...visSections]
    },

    deactivate: () => {
      container.classList.remove('editorPointer')
      camera.cleanup()
      selected = null
      camera = null
    },

    objectDestroyed: obj => {
      if ( selected === obj ) {
        editor.edit(null)
        selected = null
      }
    },
  }

  camera = editCamera({
    editController: self,
    visualizer,
    engine
  })

  container.focus()

  return self
}

export default editController
