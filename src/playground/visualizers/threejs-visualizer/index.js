import * as THREE from 'three'

import nullish from '@util/nullish'

import visualBuilder from './visual-builder'
import editController from './edit-controller'

import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer'
import { RenderPass } from 'three/examples/jsm/postprocessing/RenderPass';

import { updateObject3DTransform } from './util'

import rotation from '@util/rotation'
import vector3 from '@util/vector3'

const VisualSymbol = Symbol('Object Visual')
const ObjectSymbol = Symbol('Object Reference')

const META_KEY = 'visual'

const LoopModes = {
  once: THREE.LoopOnce,
  repeat: THREE.LoopRepeat,
  ping_pong: THREE.LoopPingPong
}

const findObjectFromScene = node => {
  let object
  let current = node
  while ( (current !== undefined) && ((object = current[ObjectSymbol]) === undefined) ) {
    current = current.parent
  }

  return current ? object : null
}

const animationPlayer = obj => async (name, opts = {}) => {
  const visual = obj[VisualSymbol]
  if ( !visual || !visual.mixer ) {
    engine.log.warning('Attempt to play animation', name, 'on object without visual:', obj.id)
    return
  }

  const { animations, mixer } = visual

  const clip = THREE.AnimationClip.findByName(animations,  name)
  if ( clip ) {
    const { fadeIn = 0.0 } = opts

    const action = mixer.clipAction(clip)
    action.loop = LoopModes[opts.loop] || LoopModes.once
    action.reset().play().fadeIn(fadeIn)
  }
}

const visualizer = (opts = {}) => engine => {
  const {
    selector,
    bgColor = 0x5b5b5b,
    near = 0.1,
    far = 500,
    fov = 70,
    shadows = false
  } = opts

  const presented = new Set()

  const scene = new THREE.Scene()
  scene.background = new THREE.Color(bgColor)

  const raycaster = new THREE.Raycaster()
  const mixers = new Set()

  const container = document.querySelector(selector)
  if ( !container ) {
    return null
  }

  const renderer = new THREE.WebGLRenderer({ antialias: true })
  renderer.setPixelRatio(window.devicePixelRatio)
  renderer.outputEncoding = THREE.sRGBEncoding
  renderer.shadowMap.enabled = shadows

  renderer.domElement.tabIndex = -1

  const mainCamera = new THREE.PerspectiveCamera(fov,
    container.clientWidth / container.clientHeight, near, far)
    
  let editingController = null
  let gridObject = null

  const renderPass = new RenderPass(scene, null)
  renderPass.camera = mainCamera

  const composer = new EffectComposer(renderer)
  composer.addPass(renderPass)
  container.appendChild(renderer.domElement)

  const adjustViewport = () => {
    container.removeChild(renderer.domElement)
    renderer.setSize(container.clientWidth, container.clientHeight, false)
    composer.setSize(container.clientWidth, container.clientHeight)
    container.appendChild(renderer.domElement)
 
    mainCamera.aspect = container.clientWidth / container.clientHeight
    mainCamera.updateProjectionMatrix()
  }
  adjustViewport()

  window.addEventListener('resize', event => {
    event.preventDefault()
    adjustViewport()
  })

  // Prevent file drops from exiting app
  const body = document.querySelector('body');
  ['dragenter', 'dragover', 'dragleave', 'drop'].forEach( name => {
    body.addEventListener(name, e => {  
      e.stopPropagation()
      e.preventDefault()
    })
  })

  const visBuilder = visualBuilder(engine, renderer, scene)

  engine.state.visual = {
    camera: {
      set position({ x, y, z }) { mainCamera.position.set(x, y, z) },

      set rotation({ x, y, z, w }) { mainCamera.quaternion.set(x, y, z, w) },
      get rotation() {
        const rot = mainCamera.quaternion
        return rotation(rot.x, rot.y, rot.z, rot.w)
      },

      rotateVector: v => {
        const nv = new THREE.Vector3(v.x, v.y, v.z)
        const rv = nv.applyQuaternion(mainCamera.quaternion)
        return vector3(rv.x, rv.y, rv.z)
      },

      rotate: ({ x, y, z, w }) => {
        const rotX = new THREE.Quaternion()
        rotX.setFromAxisAngle(new THREE.Vector3(1.0, 0.0, 0.0), x)

        const rotY = new THREE.Quaternion()
        rotY.setFromAxisAngle(new THREE.Vector3(0.0, 1.0, 0.0), y)

        mainCamera.quaternion.multiplyQuaternions(rotY, rotX)
      }
    },

    get container() { return renderer.domElement }
  }

  const self = {
    notify: obj => (obj.props.visual !== undefined),

    activated: async obj => {
      obj.extend('graphics', {
        playAnimation: animationPlayer(obj)
      })

      const visual = await visBuilder(obj.props.visual, (editingController !== null))
      if ( !visual ) {
        engine.log.warning(`Cannot create visual for object ${obj.id}`)
        return
      }

      visual.scene[ObjectSymbol] = obj
      obj[VisualSymbol] = visual
      presented.add(obj)

      scene.add(visual.root)

      if ( visual.mixer ) {
        mixers.add(visual.mixer)
      }

      self.reposition(obj)
    },

    deactivated: obj => {
      const visual = obj[VisualSymbol]
      if ( !visual ) {
        return
      }

      scene.remove(visual.root)
      delete obj[VisualSymbol]
      presented.delete(obj)
  
      if ( visual.mixer ) {
        mixers.delete(mixer)
      }

      if ( visual.cleanup ) {
        visual.cleanup()
      }
    },

    destroyed: obj => {
      if ( editingController ) {
        editingController.objectDestroyed(obj)
      }
    },

    reposition: obj => {
      const visual = obj[VisualSymbol]
      if ( !visual ) {
        return
      }

      updateObject3DTransform(visual.root, 
        obj.position, obj.rotation)
    },

    pick: ({x, y}) => {
      const posx = (x / container.clientWidth) * 2 - 1
      const posy = -(y / container.clientHeight) * 2 + 1

      raycaster.setFromCamera(new THREE.Vector2(posx, posy), mainCamera)

      const test = Array.from(presented).map( obj => obj[VisualSymbol].scene )
      const elements = raycaster.intersectObjects(test, true)

      const objs = elements.reduce( (acc, e) => {
        const o = findObjectFromScene(e.object)
        if ( o ) {
          acc.add(o)
        }

        return acc
      }, new Set() )
      
      return Array.from(objs)
    },

    update: delta => {
      mixers.forEach( m => m.update(delta) )

      presented.forEach( p => {
        const visual = p[VisualSymbol]
        if ( visual.animate ) {
          visual.animate(delta)
        }
      })

      if ( mainCamera ) {
        composer.render(delta)
      }
    },

    editors: (obj, editor) =>
      editingController.editors(obj),

    editorActivated: editor => {
      editingController = editController({
        engine,
        scene,
        visualizer: self,
        visualBuilder: visBuilder,
        renderer,
        container,
        editor
      })

      adjustViewport()
    },

    editorDeactivated: () => {
      editingController.deactivate()
      editingController = null

      adjustViewport()
    },

    editorOpened: editor => adjustViewport(),
    editorClosed: editor => adjustViewport()
  }

  return self
}

export default visualizer
