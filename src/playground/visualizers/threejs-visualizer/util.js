
export const updateObject3DTransform = (object3d, position, rotation, scale) => {
  if ( position ) {
    const { x, y, z } = position
    object3d.position.set(x, y, z)
  }

  if ( rotation ) {
    const { x, y, z, w } = rotation
    if ( w !== undefined ) {
      object3d.quaternion.set(x, y, z, w)
    } else {
      object3d.rotation.set(x, y, z)
    }
  }

  if ( scale ) {
    const { x, y, z } = scale
    object3d.scale.set(x, y, z)
  }
}

export const toColorInt = (v, defaulColor = 0xFFFFFF) => {
  if ( typeof v === 'string' && (v[0] === '#') ) {
    if ( v.startsWith('#') ) {
      return Number.parseInt(v.substr(1), 16)
    }

    return Number.parseInt(v)
  }

  return (typeof v === 'number') ? v : defaulColor
}

export default updateObject3DTransform
