import * as THREE from 'three'
import { toColorInt } from '../../util'

const DEFAULT_WIDTH = 1.0
const DEFAULT_HEIGHT = 1.0
const DEFAULT_DEPTH = 1.0

const KEY_WIDTH = ['visual', 'width']
const KEY_HEIGHT = ['visual', 'height']
const KEY_DEPTH = ['visual', 'depth']

const builder = () => {
  const editors = (obj, desc) => {
    const {
      width = DEFAULT_WIDTH,
      height = DEFAULT_HEIGHT,
      depth = DEFAULT_DEPTH
    } = desc

    return [{
      label: 'Box',
      fields: [
        { label: 'Width', type: 'number', value: width,
          edit: v => obj.set(KEY_WIDTH, v) },
        { label: 'Height', type: 'number', value: height,
          edit: v => obj.set(KEY_HEIGHT, v) },
        { label: 'Depth', type: 'number', value: depth,
          edit: v => obj.set(KEY_DEPTH, v) }
      ]
    }]
  }

  const build = desc => {
    const {
      color = 0xffff00,
      width = DEFAULT_WIDTH,
      height = DEFAULT_HEIGHT,
      depth = DEFAULT_DEPTH
    } = desc

    const material = new THREE.MeshPhongMaterial({ color: toColorInt(color) })
    const geometry = new THREE.BoxGeometry(width, height, depth)
    const mesh = new THREE.Mesh(geometry, material)

    return {
      scene: mesh,
      
      cleanup: () => {
        geometry.dispose()
        material.dispose()
      }
    }
  }

  return { build, editors }
}

export default builder
