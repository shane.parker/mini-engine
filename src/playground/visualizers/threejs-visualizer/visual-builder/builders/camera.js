
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'
import * as THREE from 'three'

import pipeline from '@util/pipeline'
import nullish from '@util/nullish'

const builder = (_engine, renderer, scene) => desc => {
  const { fov, near, far, helper } = desc

  const cam = new THREE.PerspectiveCamera( nullish(fov, 70),
    renderer.domElement.clientWidth / renderer.domElement.clientHeight,
    nullish(near, 0.1), nullish(far, 500) )

  const vis = {
    scene: cam,
    camera: desc,
    cleanup: pipeline()
  }

  const camHelper = (helper === true) ? new THREE.CameraHelper(cam) : null
  if ( camHelper ) {
    scene.add(camHelper)
    vis.animate = () => camHelper.update()
    vis.cleanup.do( () => {
      scene.remove(camHelper)
    })
  }

  return vis
}

export default builder
