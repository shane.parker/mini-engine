import * as THREE from 'three'
import { toColorInt } from '../../util'

const builder = () => desc => {
  const {
    radius = 1.0,
    height = 1.0,
    color = 0xFFFFFF,
    radialSegments = 16,
    heightSegments = 1
  } = desc

  const geometry = new THREE.CylinderGeometry(radius, radius, height, radialSegments, heightSegments);
  const material = new THREE.MeshPhongMaterial({ color: toColorInt(color) });
  const cylinder = new THREE.Mesh(geometry, material);
  
  return {
    scene: cylinder,
    cleanup: () => {
      geometry.dispose()
      material.dispose()
    }
  }
}

export default builder
