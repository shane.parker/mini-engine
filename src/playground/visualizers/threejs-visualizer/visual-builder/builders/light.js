import * as THREE from 'three'

import nullish from '@util/nullish'
import { toColorInt } from '../../util'

const builder = () => (desc, {scene, editorEnabled}) => {
  const getLight = () => {
    const {
      color,
      intensity,
      distance,
      penumbra,
      decay
    } = desc
    
    if ( desc.ambient === true ) {
      return new THREE.AmbientLight(toColorInt(desc.color))
    } else if ( desc.directional === true ) {
      return new THREE.DirectionalLight(toColorInt(desc.color))
    } else if ( typeof desc.spotlight === 'object' ) {
      return new THREE.SpotLight(toColorInt(desc.color), intensity, distance, penumbra, decay)
    }

    return new THREE.PointLight( toColorInt(color), nullish(intensity, 1),
      nullish(distance, 100), nullish(decay, 2) )
  }

  const getHelper = (lnode, desc) => {
    if ( desc.directional ) {
      return new THREE.DirectionalLightHelper(lnode, desc.helper)
    }  
    else if ( desc.spotlight ) {
      return new THREE.SpotLightHelper(lnode)
    }

    return new THREE.PointLightHelper(lnode, desc.helper);
  }

  const lnode = getLight()
  if ( !lnode ) {
    return
  }

  const vis = {
    scene: lnode
  }

  if ( editorEnabled ) {
    const lightHelper = getHelper(lnode, desc)
    lnode.add(lightHelper);
    lightHelper.updateMatrix()
    lightHelper.update()

    vis.animate = () => {
      lightHelper.update()
    }

    vis.cleanup = () => {
      console.log('Cleanup')
      scene.remove(lightHelper)
      lightHelper.dispose()
    }
  }

  return vis
}

export default builder
