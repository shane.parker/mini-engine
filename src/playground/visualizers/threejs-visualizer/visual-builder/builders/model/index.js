import * as THREE from 'three'

import modelLoader from './model-loader'

const loopModes = {
  once: THREE.LoopOnce,
  repeat: THREE.LoopRepeat,
  ping_pong: THREE.LoopPingPong
}

const builder = engine => {
  const loader = modelLoader(engine)

  const self = async desc => {
    const { model } = desc

    const gltf = await loader.load(`assets/models/${model}.glb`)
    if ( !gltf ) {
      return
    }

    const { animations = [], scene } = gltf
    const clonedScene = scene.clone()

    engine.log.debug('Model structure:', gltf)

    const mixer = (animations.length > 0) ? new THREE.AnimationMixer(clonedScene) : undefined
    if ( mixer ) {
      const animation = obj.get('animation')
      if ( animation ) {
        const { name: animationName, timeScale = 1.0, loop = 'repeat' } =
          (typeof animation === 'string') ? { name: animation } : animation

        if ( animationName ) {
          const clip = THREE.AnimationClip.findByName(animations,  animationName)
          if ( clip ) {
            const action = mixer.clipAction(clip).play()
            action.loop = loopModes[loop]
            mixer.timeScale = timeScale
          }
        }
      }
    }

    return {
      scene: clonedScene,
      animations,
      mixer
    }
  }

  return self
}

export default builder
