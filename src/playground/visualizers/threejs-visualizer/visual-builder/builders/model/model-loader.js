import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader'

const modelLoader = engine => {
  const loader = new GLTFLoader()
  const modelMap = {}

  return {
    load: name => new Promise( (resolve, reject) => {
      const entry = modelMap[name] || {pending: []}
      if ( entry.model ) {
        return resolve(cached.model)
      }

      const pending = [...entry.pending, {resolve, reject}]
      modelMap[name] = {...entry, pending}

      if (pending.length > 1) {
        engine.log.debug(`Waiting for pending model ${name} to load...`)
        return
      }

      engine.log.debug(`Loading model ${name}`)
      loader.load(name, gltf => {
        engine.log.debug(`Finished loading model ${name}`)

        const entry = modelMap[name]
        if ( entry ) {
          entry.pending.forEach(e => e.resolve(gltf))
        }

        modelMap[name] = {model: gltf}
      }, undefined, err => {
        engine.log.error(err)

        const entry = modelMap[name]
        if ( entry ) {
          entry.pending.forEach(e => e.reject())
        }

        delete modelMap[name]
      })
    }),
    
    flush: () => {
      Object.keys(modelMap).forEach( k => {
        const model = modelMap[k].model
        if ( model ) {
          obj.dispose()
        }

        delete modelMap[k]
      })
    }
  }
}

export default modelLoader
