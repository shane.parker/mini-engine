import * as THREE from 'three'
import { toColorInt } from '../../util'

const builder = () => desc => {
  const {
    color = 0x00ff00,
    width = 1.0,
    height = 1.0,
    segments = 1
  } = desc

  const material = new THREE.MeshPhongMaterial({ color: toColorInt(color) })
  const geometry = new THREE.PlaneGeometry(width, height, segments)
  const mesh = new THREE.Mesh(geometry, material)

  return {
    scene: mesh,
    cleanup: () => {
      geometry.dispose()
      material.dispose()
    }
  }
}

export default builder
