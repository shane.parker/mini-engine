import * as THREE from 'three'

import { toColorInt } from '../../util'

const DEFAULT_RADIUS = 1.0
const DEFAULT_SEGMENTS_X = 32
const DEFAULT_SEGMENTS_Y = 32

const KEY = 'visual'

const builder = () => {
  const editors = (obj, desc) => {
    const {
      radius = DEFAULT_RADIUS,
      segmentsX = DEFAULT_SEGMENTS_X,
      segmentsY = DEFAULT_SEGMENTS_Y
    } = desc

    return [{
      label: 'Sphere',
      fields: [
        { label: 'Radius', type: 'number', value: radius,
          edit: v => obj.set(`${key}.radius`, v) },
        { label: 'Segments X', type: 'number', value: segmentsX,
          edit: v => obj.set(`${key}.segmentsX`, v) },
        { label: 'Segments Y', type: 'number', value: segmentsY,
          edit: v => obj.set(`${key}.segmentsY`, v) },
      ]
    }]
  }

  const build = desc => {
    const {
      color = 0xff0000,
      radius = 1.0,
      segmentsX = 32,
      segmentsY = 32
    } = desc

    const material = new THREE.MeshPhongMaterial({ color: toColorInt(color) })
    const geometry = new THREE.SphereGeometry(radius, segmentsX, segmentsY)
    const mesh = new THREE.Mesh(geometry, material)

    return {
      scene: mesh,
      cleanup: () => {
        geometry.dispose()
        material.dispose()
      }
    }
  }

  return { build, editors }
}

export default builder
