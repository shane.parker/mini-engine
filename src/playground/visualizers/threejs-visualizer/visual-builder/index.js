
import * as THREE from 'three'

import { updateObject3DTransform } from '../util'

import cylinderBuilder from './builders/cylinder'
import modelBuilder from './builders/model'
import lightBuilder from './builders/light'
import cameraBuilder from './builders/camera'
import planeBuilder from './builders/plane'
import sphereBuilder from './builders/sphere'
import boxBuilder from './builders/box'

const prepare = (v, desc, {editorEnabled}) => {
  if ( !v ) {
    // TODO: Add default object mesh
    return { scene: Object3D() }
  }

  const { castShadow, receiveShadow, helper } = desc

  v.scene.castShadow = (castShadow === true)
  v.scene.receiveShadow = (receiveShadow === true)

  /*if ( editorEnabled ) {
    const axesHelper = new THREE.AxesHelper(helper)
    axesHelper.material.depthTest = false
    v.scene.add(axesHelper)
  }*/

  const setupRoot = () => {
    if ( desc.position || desc.rotation || desc.scale ) {
      const rootNode = new THREE.Object3D()

      rootNode.add(v.scene)
      updateObject3DTransform(v.scene, 
        desc.position, desc.rotation, desc.scale)

      return rootNode
    }

    return v.scene
  }

  v.root = setupRoot()

  return v
}

const visualBuilder = (engine, renderer, scene) => {
  const builders = {
    camera: cameraBuilder(engine, renderer, scene),
    cylinder: cylinderBuilder(),
    model: modelBuilder(engine),
    light: lightBuilder(engine),
    plane: planeBuilder(),
    sphere: sphereBuilder(),
    box: boxBuilder()
  }
  
  const builder = ({ type, ...desc }, editorEnabled) => {
    const entry = builders[type] || {}

    const build = (typeof entry === 'function') ? entry : entry.build
    if ( !build ) {
      engine.log.error('Invalid visual type:', type)
      return
    }

    const context = {
      scene,
      editorEnabled
    }

    return prepare(build(desc, context), desc, context)
  }

  builder.editors = (obj, type, desc) => {
    const { editors } = builders[type] || {}
    return editors ? editors(obj, desc) : []
  }

  return builder
}

export default visualBuilder
