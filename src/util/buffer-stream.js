export const bufferStream = (buffer, opts = {}) => {
  let { offset = 0, view = new DataView(buffer) } = opts
  const { expandFactor = 2 } = opts

  const self = {
    writeUint32: value => (view.setUint32(offset, value), offset += 4, self),
    writeFloat32: value => (view.setFloat32(offset, value), offset += 4, self),

    readUint32: () => {
      const val = view.getUint32(offset)
      offset += 4
      return val
    },

    readFloat32: () => {
      const val = view.getFloat32(offset)
      offset += 4
      return val
    },

    reserve: count => {
      if ( (buffer.byteLength - offset) < count ) {
        const nbuffer = new ArrayBuffer((buffer.byteLength * expandFactor) + count)
        new Uint8Array(nbuffer).set(buffer)
        view = new DataView(nbuffer)
        buffer = nbuffer
      }

      return self
    },

    offset: () => offset,
    buffer: () => buffer
  }

  return self
}

export default bufferStream
