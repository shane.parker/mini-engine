
const compose = (fn1, ...fn2) => (...params) =>
  (fn2.length > 0) ? compose(...fn2)(fn1(...params)) : fn1(...params)

export default compose
