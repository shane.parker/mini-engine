const eventListenerSet = (defaultObject, listeners) => {
  const handlerList = Array.isArray(defaultObject)
    ? [...defaultObject]
    : [...listeners]

  if ( !listeners ) {
    defaultObject = null
  }

  const self = {
    cleanup: () =>
      (handlerList.forEach( ({object = defaultObject, event, events, listener}) =>
        (events ? events : [event]).forEach( e => object.removeEventListener(e, listener) )), self),

    setup: () =>
      (handlerList.forEach( ({object = defaultObject, event, events, listener}) =>
        (events ? events : [event]).forEach( e => object.addEventListener(e, listener) )), self),
  }

  return self
}

export default eventListenerSet
