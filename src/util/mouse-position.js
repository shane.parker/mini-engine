
const mousePosition = (element, event) => {
  const r = element.getBoundingClientRect()
  const x = window.pageXOffset + event.clientX
  const y = window.pageYOffset + event.clientY

  return { x: x - r.left, y: y - r.top }
}

export default mousePosition
