
const nullish = (v1, v2) =>
    ((v1 !== undefined) && (v1 !== null)) ? v1 : v2

export default nullish
