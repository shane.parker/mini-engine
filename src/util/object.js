import nullish from '@util/nullish'

export const clone = v => {
  if ( Array.isArray(v) ) {
    return v.map(e => clone(e))
  } else if ( v && (typeof v === 'object') ) {
    return Object.entries(v).reduce( (acc, [k, v]) =>
      (acc[k] = clone(v), acc), {})
  }

  return v
}

export const setObjectPath = (obj, path, value) => {
  const comps = Array.isArray(path) ? path : path.split('.')
  const lastIndex = comps.length - 1

  return comps.reduce( (acc, k, idx) => 
    (idx !== lastIndex)
      ? (acc[k] = nullish(acc[k], {}))
      : (acc[k] = value), obj)
}

export const getObjectPath = (current, path, defaultValue) => {
  const comps = Array.isArray(path) ? path : path.split('.')

  for ( let x = 0; x < comps.length; x++ ) {
    const next = current[comps[x]]
    if ( next === undefined ) {
      return defaultValue
    }

    current = next
  }

  return current
}

export const dynamicStore = (opts = {}) => {
  const {
    load = getObjectPath,
    store = setObjectPath,
    root = {}
  } = opts

  const settings = (obj = {}, path = []) => new Proxy(obj, {
    set: (_target, prop, value) => {
      store(root, [...path, prop], value)
      return true
    },

    get: (_target, prop) => {
      const npath = [...path, prop]
      const dval = value => {
        if ( value !== undefined ) {
          return store(root, npath, value)
        } else {
          return load(root, npath)
        }
      }
      
      return settings(dval, npath)
    },

    ownKeys: target => Reflect.ownKeys(target)
  })

  return settings()
}

export const value = v =>
  (typeof v === 'function') ? v() : v

const resolveVal = (root, path) => {
  const comps = Array.isArray(path) ? path : path.split('.')
  const end = comps.length-1
  const name = comps[end]

  const obj = getObjectPath(root, comps.slice(0, end), {})
  const fn = v =>
    (v !== undefined) ? (fn.link = undefined, obj[name] = v)
      : ((fn.link && fn.link()) || obj[name])

  fn.paramName = name
  return fn
}

export const valueResolver = root => (...paths) => {
  if ( paths.length === 0 ) {
    paths = Object.keys(root)
  }

  return paths.reduce( (acc, p) => {
    const {path, name} = (typeof p === 'object') ? p
      : {path: p, name: p}

    acc[name] = resolveVal(root, path)
    return acc
  }, {})
}
