
const pipeline = (...funcs) => {
  const chain = [...funcs]

  const f = (first, ...params) => {
    let r = first ? first(...params) : undefined
    for ( const next of chain ) {
      r = next(r)
    }

    return r
  }

  f.do = (...fn) => fn.forEach( f => chain.push(f) )

  return f
}

export default pipeline
