
const random = (seed = Date.now()) => {
  const max = Math.pow(2, 32)

  return {
    seed: val =>
      seed = val || Math.round(0.67828283 * max),

    number: () => {
      seed += (seed * seed) | 5
      return (seed >>> 32) / max
    }
  }
}

export const randomRange = (min, max) => Math.floor(Math.random() * (max - min) ) + min

export const randomElement = l => l[Math.trunc(randomRange(0, l.length))]

export default random
