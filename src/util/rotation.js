
const rotation = (x = 0.0, y = 0.0, z = 0.0, w = 1.0) => ({
  x, y, z, w
})

export default rotation
