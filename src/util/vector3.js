
export const vector3 = (x = 0.0, y = 0.0, z = 0.0) => ({x, y, z})

export const add = (v1, v2) => ({
  x: v1.x + v2.x,
  y: v1.y + v2.y,
  z: v1.z + v2.z
})

export const sub = (v1, v2) => ({
  x: v1.x - v2.x,
  y: v1.y - v2.y,
  z: v1.z - v2.z
})

export const mul = (v1, v2) => ({
  x: v1.x * v2.x,
  y: v1.y * v2.y,
  z: v1.z * v2.z
})

export const scale = ({x, y, z}, f) => ({
  x: x * f,
  y: y * f,
  z: z * f
})

export const magnitude = ({x, y, z}) => Math.sqrt((x*x) + (y*y) + (z*z))

export const magnitude2 = ({x, y, z}) => (x*x) + (y*y) + (z*z)

export const distance = (v1, v2) => {
  const t = sub(v2, v1)
  return magnitude(t)
}

export const distance2 = (v1, v2) => {
  const t = sub(v2, v1)
  return magnitude2(t)  
}

export const normalize = v => {
  const l = magnitude(v)
  if ( l <= 0.0 ) {
    return v
  }

  return {
    x: v.x / l,
    y: v.y / l,
    z: v.z / l
  }
}

export const setMagnitude = (v, l) => {
  const n = normalize(v)
  
  return {
    x: n.x * l,
    y: n.y * l,
    z: n.z * l
  }
}

export const interpolate = (v1, v2, t) => {
  const s = (1.0 - t)

  const tx = s * v1.x + t * v2.x
  const ty = s * v1.y + t * v2.y
  const tz = s * v1.z + t * v2.z

  return { x: tx, y: ty, z: tz }
}

Object.assign(vector3, {
  add,
  sub,
  mul,
  magnitude,
  magnitude2,
  interpolate,
  setMagnitude,
  normalize,
  scale
})

export default vector3
