
const path = require('path');

module.exports = {
  resolve: {
    alias: {
      '@util': path.resolve(__dirname, 'src/util/')
    }
  },
  node: {
    fs: 'empty'
  },
  module: {
    rules: [
      {
        test: /\.worker\.js$/,
        use: {
          loader: 'worker-loader',
          options: {
            inline: true,
            fallback: false
          }
        }
      }
    ]
  }
}
